package models

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes}


case class SwapiFilms(
  title: String = "A New Hope",
) {
  val created: String = "2014-12-10T14:23:31.880000Z"
  val edited = "2015-04-11T09:46:52.774897Z"
  val url: String = "https://swapi.co/api/films/1/"
}


object SwapiFilms {

  def custom_unapply(arg: SwapiFilms): Option[(String, String, String, String)] = {
    Some((
      arg.title,
      arg.created,
      arg.edited,
      arg.url
    ))
  }

  implicit val messageWrites: Writes[SwapiFilms] = (
    (JsPath \ "title").write[String] and
      (JsPath \ "created").write[String] and
      (JsPath \ "edited").write[String] and
      (JsPath \ "url").write[String]
    ) (unlift(custom_unapply))
}

