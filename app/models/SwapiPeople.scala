package models

import play.api.libs.json._
import play.api.libs.functional.syntax._


case class SwapiPeople(
  name: String = "Luke Skywalker",
  height: String = "172",
  mass: String = "77",
  hair_color: String = "blond",
  skin_color: String = "fair",
  eye_color: String = "blue",
  birth_year: String = "19BBY",
  gender: String = "male",
  homeworld: String = "https://swapi.co/api/planets/1/",
  films: List[SwapiFilms] = List(SwapiFilms(), SwapiFilms()),
  species: List[String] = List(
    "https://swapi.co/api/species/1/"
  ),
  vehicles: List[String] = List(
    "https://swapi.co/api/vehicles/14/",
    "https://swapi.co/api/vehicles/30/"
  ),
  starships: List[String] = List(
    "https://swapi.co/api/starships/12/",
    "https://swapi.co/api/starships/22/"
  )
) {
  val created: String = "2014-12-09T13:50:51.644000Z"
  val edited: String = "2014-12-20T21:17:56.891000Z"
  val url: String = "https://swapi.co/api/people/1/"
}


object SwapiPeople {

  def custom_unapply(arg: SwapiPeople): Option[(
    String, String, String, String, String, String, String, String, String,
      List[SwapiFilms], List[String], List[String], List[String], String, String, String
    )] = {
    Some((
      arg.name,
      arg.height,
      arg.mass,
      arg.hair_color,
      arg.skin_color,
      arg.eye_color,
      arg.birth_year,
      arg.gender,
      arg.homeworld,
      arg.films,
      arg.species,
      arg.vehicles,
      arg.starships,
      arg.created,
      arg.edited,
      arg.url
    ))
  }


  // TODO and if i want other deserialization view ?
  implicit val swapiPeopleWrites: Writes[SwapiPeople] = (
    (JsPath \ "name").write[String] and
      (JsPath \ "height").write[String] and
      (JsPath \ "mass").write[String] and
      (JsPath \ "hair_color").write[String] and
      (JsPath \ "skin_color").write[String] and
      (JsPath \ "eye_color").write[String] and
      (JsPath \ "birth_year").write[String] and
      (JsPath \ "gender").write[String] and
      (JsPath \ "homeworld").write[String] and
      (JsPath \ "films").write[List[SwapiFilms]] and
      (JsPath \ "species").write[List[String]] and
      (JsPath \ "vehicles").write[List[String]] and
      (JsPath \ "starships").write[List[String]] and
      (JsPath \ "created").write[String] and
      (JsPath \ "edited").write[String] and
      (JsPath \ "url").write[String]

    ) (unlift(custom_unapply))


  def custom_unapply_short(arg: SwapiPeople): Option[(String, String)] = Some((arg.name, arg.url))

  val swapiPeopleWritesShort: Writes[SwapiPeople] = (
    (JsPath \ "name").write[String] and
      (JsPath \ "url").write[String]
    ) (unlift(custom_unapply_short))
}
