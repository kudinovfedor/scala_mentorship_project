package models
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._


case class Item(author: String, text: String, status: String = "active")


object Item {
  //  def apply(author: String, text: String, status: String): Item = new Item(author, text, status)
  def apply(author: String, text: String): Item = new Item(author, text)  // todo how to do correct apply overload

  //  implicit val itemImplicitReads = Json.reads[Item]
  implicit val itemImplicitWrites = Json.writes[Item]

  val itemReads = (
    (JsPath \ "author").read[String] and
      (JsPath \ "text").read[String]
    )(Item.apply _)

}
