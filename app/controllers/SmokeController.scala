package controllers

import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._


@Singleton
class SmokeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def smoke(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val json = Json.toJson(Map("status" -> "success"))
    Ok(json).as("application/json")
  }
}





