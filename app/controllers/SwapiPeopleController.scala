package controllers

import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._

import models.{SwapiPeople, SwapiFilms}

@Singleton
class SwapiPeopleController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def preparePeople(): List[SwapiPeople] = {

    val films: List[SwapiFilms] = List(
      SwapiFilms(title="First Film"),
      SwapiFilms(title="Second Film")
    )
    List(SwapiPeople(films=films), SwapiPeople())
  }

  def getAll(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val persons = preparePeople()
    val json = Json.toJson(persons)
    Ok(json).as("application/json")
  }

  def getAllShort(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val persons = preparePeople()
    val json = Json.toJson(persons.map(Json.toJson(_)(SwapiPeople.swapiPeopleWritesShort)))
    Ok(json).as("application/json")
  }

}
