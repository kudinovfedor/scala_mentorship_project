package controllers

import javax.inject._
import play.api.mvc._
import models.Item
import play.api.libs.json.Json


@Singleton
class ItemController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def items(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val json = Json.toJson(List(Item(author="Me", text="Some description")))
    Ok(json).as("application/json")
  }

  def create(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val item = Json.fromJson[Item](request.body.asJson.get)(Item.itemReads).get
    val json = Json.toJson(List(item))
    Ok(json).as("application/json")
  }
}
